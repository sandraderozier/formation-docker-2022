
# 
git clone https://forgemia.inra.fr/sandraderozier/formation-docker-2022.git
cd formation-docker-2022
# build
docker build -t shinyapp:1.0.0  ./
docker run --rm -it -p 3838:3838 shinyapp:1.0.0
