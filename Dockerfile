FROM rocker/shiny-verse:4.1.1

COPY shiny_docker/app.R  /

RUN echo "local(options(shiny.port = 3838, shiny.host = '0.0.0.0'))" >> /usr/local/lib/R/etc/Rprofile.site

EXPOSE 3838

CMD ["R", "-e shiny::runApp()"]